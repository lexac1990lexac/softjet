<?php

namespace App\Services;

use App\Interfaces\Service\GameServiceInterface;
use App\Interfaces\Repository\GameRepositoryInterface;

class GameService implements GameServiceInterface
{
    public function __construct(
        private GameRepositoryInterface $gameRepository
    ) {}

    public function store(array $data)
    {
        return $this->gameRepository->store($data);
    }

    public function update(array $data, int $gameId)
    {
        return $this->gameRepository->update($data, $gameId);
    }

    public function delete(int $gameId)
    {
        return $this->gameRepository->delete($gameId);
    }

    public function syncGenre($game, array $genres)
    {
        return $this->gameRepository->syncGenre($game, $genres);
    }
}