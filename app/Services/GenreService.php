<?php

namespace App\Services;

use App\Interfaces\Service\GenreServiceInterface;
use App\Interfaces\Repository\GenreRepositoryInterface;

class GenreService implements GenreServiceInterface
{
    public function __construct(
        private GenreRepositoryInterface $genreRepository
    ) {}

    public function store(array $data)
    {
        return $this->genreRepository->store($data);
    }

    public function update(array $data, int $gameId)
    {
        return $this->genreRepository->update($data, $gameId);
    }

    public function delete(int $gameId)
    {
        return $this->genreRepository->delete($gameId);
    }
}