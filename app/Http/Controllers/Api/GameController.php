<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\Game\StoreGameRequest;
use App\Http\Requests\Api\Game\UpdateGameRequest;
use App\Http\Requests\Api\Game\SyncRequest;
use App\Interfaces\Service\GameServiceInterface;
use App\Interfaces\Repository\GameRepositoryInterface;
use App\Http\Resources\GameResource;
use Illuminate\Http\Response;
use App\Models\Game;

class GameController extends Controller
{
    public function __construct(
        private GameServiceInterface $gameService,
        private GameRepositoryInterface $gameRepository
    ) {}

    /**
     * Get List of Games
     * @OA\Get (
     *     path="/api/games",
     *     tags={"Games"},
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 type="array",
     *                 property="data",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(
     *                         property="id",
     *                         type="number",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         example="Game name"
     *                     ),
     *                     @OA\Property(
     *                         property="description",
     *                         type="string",
     *                         example="Game description"
     *                     ),
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function index()
    {
        $list = $this->gameRepository->findAll();

        return GameResource::collection($list);
    }

    /**
     * Create new Game
     * @OA\Post(
     *      path="/api/games",
     *      tags={"Games"},
     *      description="Create new Game",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="name",
     *                  type="string",
     *                  example="New game",
     *              ),
     *              @OA\Property(
     *                  property="description",
     *                  type="string",
     *                  example="New game description",
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="New game created",
     *          @OA\JsonContent(
     *             @OA\Property(
     *                 type="object",
     *                 property="data",
     *                 @OA\Property(
     *                     property="id",
     *                     type="number",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="New game"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string",
     *                     example="New game description"
     *                 ),
     *             )
     *         )
     *       ),
     *       @OA\Response(
     *          response=422,
     *          description="Validation error",
     *       )
     * )
     */
    public function store(StoreGameRequest $request)
    {
        $data = $request->validated();

        $game = $this->gameService->store($data);

        return GameResource::make($game);
    }

    /**
     * Get Game by ID
     * @OA\Get (
     *     path="/api/games/{id}",
     *     tags={"Games"},
     *     @OA\Parameter(
     *          name="id",
     *          description="Game id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"         
     *          ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 type="object",
     *                 property="data",
     *                 @OA\Property(
     *                     property="id",
     *                     type="number",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="Game name"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string",
     *                     example="Game description"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *        response=404,
     *        description="Game not found",
     *        @OA\JsonContent(
     *            @OA\Property(
     *                type="string",
     *                property="error",
     *                example="Game with ID {id} not found"
     *            )
     *        )
     *     )
     * )
     */
    public function show(Game $game)
    {
        return GameResource::make($game);
    }

    /**
     * Update an existing game
     * @OA\Put(
     *      path="/api/games/{id}",
     *      tags={"Games"},
     *      description="Create new Game",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="name",
     *                  type="string",
     *                  example="New name",
     *              ),
     *              @OA\Property(
     *                  property="description",
     *                  type="string",
     *                  example="New description",
     *              )
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="id",
     *          description="Game id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"         
     *          ),
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Game updated",
     *          @OA\JsonContent(
     *             @OA\Property(
     *                 type="object",
     *                 property="data",
     *                 @OA\Property(
     *                     property="id",
     *                     type="number",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="New name"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string",
     *                     example="New description"
     *                 ),
     *             )
     *         )
     *       ),
     *       @OA\Response(
     *          response=422,
     *          description="Validation error",
     *       ),
     *       @OA\Response(
     *          response=404,
     *          description="Game not found",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  type="string",
     *                  property="error",
     *                  example="Game with ID {id} not found"
     *              )
     *          )
     *       )
     * )
     */
    public function update(UpdateGameRequest $request, int $id)
    {   
        $data = $request->validated();

        $game = $this->gameService->update($data, $id);

        return GameResource::make($game);
    }

    /**
     * Delete game by ID
     * @OA\Delete (
     *     path="/api/games/{id}",
     *     tags={"Games"},
     *     @OA\Parameter(
     *          name="id",
     *          description="Game id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"         
     *          ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Deleted",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 type="string",
     *                 property="message",
     *                 example="Game deleted"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *        response=404,
     *        description="Game not found",
     *        @OA\JsonContent(
     *            @OA\Property(
     *                type="string",
     *                property="error",
     *                example="Game with ID {id} not found"
     *            )
     *        )
     *     )
     * )
     */
    public function destroy(Game $game)
    {
        $game->delete();

        return response()->json([
            'message' => 'Game deleted'
        ], Response::HTTP_OK);
    }

    /**
     * Sync game with genres
     * @OA\Put(
     *      path="/api/games/{id}/sync",
     *      tags={"Games"},
     *      description="Sync game with genres",
     *      @OA\Parameter(
     *          name="id",
     *          description="Game id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"         
     *          ),
     *     ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="genres",
     *                  type="array",
     *                  @OA\Items(
     *                      example="1"
     *                 )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Genre detached",
     *          @OA\JsonContent(
     *             @OA\Property(
     *                 type="string",
     *                 property="message",
     *                 example="Genre detached"
     *             )
     *         )
     *       ),
     *       @OA\Response(
     *          response=422,
     *          description="Validation error",
     *       ),
     * )
     */
    public function sync(SyncRequest $request, int $game)
    {
        $this->gameService->syncGenre($game, $request->genres);

        return response()->json([
            'message' => 'ok'
        ], Response::HTTP_OK);
    }
}
