<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\Genre\StoreGenreRequest;
use App\Http\Requests\Api\Genre\UpdateGenreRequest;
use App\Interfaces\Service\GenreServiceInterface;
use App\Interfaces\Repository\GenreRepositoryInterface;
use App\Http\Resources\GenreResource;
use Illuminate\Http\Response;
use App\Models\Genre;

class GenreController extends Controller
{
    public function __construct(
        private GenreServiceInterface $genreService,
        private GenreRepositoryInterface $genreRepository
    ) {}

    /**
     * Get List of Genres
     * @OA\Get (
     *     path="/api/genres",
     *     tags={"Genres"},
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 type="array",
     *                 property="data",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(
     *                         property="id",
     *                         type="number",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         example="Genre name"
     *                     ),
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function index()
    {
        $list = $this->genreRepository->findAll();

        return GenreResource::collection($list);
    }

    /**
     * Create new Genre
     * @OA\Post(
     *      path="/api/genres",
     *      tags={"Genres"},
     *      description="Create new Genre",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="name",
     *                  type="string",
     *                  example="New Genre",
     *              ),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="New genre created",
     *          @OA\JsonContent(
     *             @OA\Property(
     *                 type="object",
     *                 property="data",
     *                 @OA\Property(
     *                     property="id",
     *                     type="number",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="New Genre"
     *                 ),
     *             )
     *         )
     *       ),
     *       @OA\Response(
     *          response=422,
     *          description="Validation error",
     *       )
     * )
     */
    public function store(StoreGenreRequest $request)
    {
        $data = $request->validated();

        $genre = $this->genreService->store($data);

        return GenreResource::make($genre);
    }

    /**
     * Get Genre by ID
     * @OA\Get (
     *     path="/api/genres/{id}",
     *     tags={"Genres"},
     *     @OA\Parameter(
     *          name="id",
     *          description="Genre id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"         
     *          ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 type="object",
     *                 property="data",
     *                 @OA\Property(
     *                     property="id",
     *                     type="number",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="Genre name"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *        response=404,
     *        description="Genre not found",
     *        @OA\JsonContent(
     *            @OA\Property(
     *                type="string",
     *                property="error",
     *                example="Genre with ID {id} not found"
     *            )
     *        )
     *     )
     * )
     */
    public function show(Genre $genre)
    {
        return GenreResource::make($genre);
    }

    /**
     * Update an existing genre
     * @OA\Put(
     *      path="/api/genres/{id}",
     *      tags={"Genres"},
     *      description="Create new Genre",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="name",
     *                  type="string",
     *                  example="New name",
     *              ),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="id",
     *          description="Genre id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"         
     *          ),
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Genre updated",
     *          @OA\JsonContent(
     *             @OA\Property(
     *                 type="object",
     *                 property="data",
     *                 @OA\Property(
     *                     property="id",
     *                     type="number",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="New genre"
     *                 ),
     *             )
     *         )
     *       ),
     *       @OA\Response(
     *          response=422,
     *          description="Validation error",
     *       ),
     *       @OA\Response(
     *          response=404,
     *          description="Genre not found",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  type="string",
     *                  property="error",
     *                  example="Genre with ID {id} not found"
     *              )
     *          )
     *       )
     * )
     */
    public function update(UpdateGenreRequest $request, $id)
    {   
        $data = $request->validated();

        $genre = $this->genreService->update($data, $id);

        return GenreResource::make($genre);
    }

    /**
     * Delete genre by ID
     * @OA\Delete (
     *     path="/api/genres/{id}",
     *     tags={"Genres"},
     *     @OA\Parameter(
     *          name="id",
     *          description="Genre id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"         
     *          ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Deleted",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 type="string",
     *                 property="message",
     *                 example="Genre deleted"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *        response=404,
     *        description="Genre not found",
     *        @OA\JsonContent(
     *            @OA\Property(
     *                type="string",
     *                property="error",
     *                example="Genre with ID {id} not found"
     *            )
     *        )
     *     )
     * )
     */
    public function destroy(Genre $genre)
    {
        $genre->delete();

        return response()->json([
            'message' => 'Genre deleted'
        ], Response::HTTP_OK);
    }
}
