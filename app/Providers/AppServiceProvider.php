<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

// Services
use App\Services\GameService;
use App\Services\GenreService;

// Interfaces
use App\Interfaces\Service\GameServiceInterface;
use App\Interfaces\Service\GenreServiceInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GameServiceInterface::class, GameService::class);
        $this->app->bind(GenreServiceInterface::class, GenreService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
