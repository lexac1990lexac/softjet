<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

// Repository
use App\Repository\GameRepository;
use App\Repository\GenreRepository;

// Interfaces
use App\Interfaces\Repository\GameRepositoryInterface;
use App\Interfaces\Repository\GenreRepositoryInterface;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GameRepositoryInterface::class, GameRepository::class);
        $this->app->bind(GenreRepositoryInterface::class, GenreRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
