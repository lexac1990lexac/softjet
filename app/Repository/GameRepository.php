<?php

namespace App\Repository;

use App\Interfaces\Repository\GameRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Game;
use DB;

class GameRepository implements GameRepositoryInterface
{
    public function findAll(): Collection
    {
        return Game::all();
    }

    public function findById(int $id): ?Game
    {
        return Game::find($id);
    }

    public function store(array $data): ?Game
    {
        return Game::create([
            'name' => $data['name'],
            'description' => $data['description']
        ]);
    }

    public function update(array $data, int $id): ?Game
    {
        $game = $this->findById($id);

        if(!$game)
        {
            return null;
        }

        $game->update([
            'name' => $data['name'],
            'description' => $data['description']
        ]);

        return $game;
    }

    public function delete(int $id): bool
    {
        return Game::where('id', $id)
            ->delete();
    }

    public function syncGenre(int $gameId, array $genres)
    {
        $game = $this->findById($gameId);

        $game->genres()->sync($genres);
    }
}