<?php

namespace App\Repository;

use App\Interfaces\Repository\GenreRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Genre;

class GenreRepository implements GenreRepositoryInterface
{
    public function findAll(): Collection
    {
        return Genre::all();
    }

    public function findById(int $id): ?Genre
    {
        return Genre::find($id);
    }

    public function store(array $data): ?Genre
    {
        return Genre::create([
            'name' => $data['name'],
        ]);
    }

    public function update(array $data, int $id): ?Genre
    {
        $genre = $this->findById($id);

        if(!$genre)
        {
            return null;
        }

        $genre->update([
            'name' => $data['name'],
        ]);

        return $genre;
    }

    public function delete(int $id): bool
    {
        return Genre::where('id', $id)
            ->delete();
    }
}