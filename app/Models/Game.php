<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'description'
    ];
    
    public $timestamps = false;

    public function genres()
    {
        return $this->belongsToMany(Genre::class)->withPivot('genre_id', 'game_id');
    }
}
