<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name'
    ];
    
    public $timestamps = false;

    public function genres()
    {
        return $this->belongsToMany(Game::class)->withPivot('game_id', 'genre_id');
    }
}
